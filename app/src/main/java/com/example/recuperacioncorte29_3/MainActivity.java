package com.example.recuperacioncorte29_3;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText txtFecha, txtTemCel, txtTemFa;
    private Button btnConvertir, btnGuardar, btnRegistro, btnSalir, btnLimpiar;

    private Aplicacion app;
    private Temperatura Temp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.instanciarComponentes();
        this.asignarMetodosClic();

        this.app = (Aplicacion) getApplication();
        this.Temp = new Temperatura();
    }

    private void instanciarComponentes()
    {
        this.txtFecha = findViewById(R.id.txtFecha);
        this.txtTemCel = findViewById(R.id.txtTemCel);
        this.txtTemFa = findViewById(R.id.txtTemFa);

        this.btnConvertir = findViewById(R.id.btnConvertir);
        this.btnGuardar = findViewById(R.id.btnGuardar);
        this.btnRegistro = findViewById(R.id.btnRegistro);
        this.btnSalir = findViewById(R.id.btnSalir);
        this.btnLimpiar = findViewById(R.id.btnLimpiar);
    }

    private void asignarMetodosClic()
    {
        this.btnConvertir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {clic_btnConvertir();}
        });
        this.btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {clic_btnGuardar();}
        });
        this.btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {clic_btnVerHistorial();}
        });
        this.btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {clic_btnLimpiar();}
        });
        this.btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {clic_btnSalir();}
        });
    }

    private boolean validarCampos()
    {
        String strFecha = this.txtFecha.getText().toString().trim();
        String strCelsius = this.txtTemCel.getText().toString().trim();

        return !strFecha.isEmpty() && !strCelsius.isEmpty();
    }

    private void clic_btnConvertir()
    {
        if(!validarCampos()){
            Toast.makeText(this, "Ingresar todos los campos.", Toast.LENGTH_SHORT).show();
            return;
        }

        String Fecha = new String(this.txtFecha.getText().toString());
        float Celsius = Float.parseFloat(this.txtTemCel.getText().toString().trim());

        if(!(Celsius > 0.0f)){
            Toast.makeText(this, "Ingresar un peso valido.", Toast.LENGTH_SHORT).show();
            return;
        }

        this.Temp = new Temperatura(1, Celsius, Fecha);

        this.txtTemFa.setText(Math.round(Temp.getFahrenheit() * 100.0)/100.0  + "");
    }

    private void clic_btnGuardar()
    {
        if(this.Temp.getId() != 1){
            Toast.makeText(this, "Favor de Convertir antes los Celsius", Toast.LENGTH_SHORT).show();
            return;
        }

        app.agregarTemperatura(Temp);

        Toast.makeText(this, "Se agrego Temperatura", Toast.LENGTH_SHORT).show();

        this.txtFecha.setText("");
        this.txtTemCel.setText("");

        this.Temp = new Temperatura();
    }

    private void clic_btnVerHistorial(){
        Intent intent = new Intent(MainActivity.this, historialActivity.class);
        startActivityForResult(intent, 0);
    }

    private void clic_btnLimpiar(){
        txtFecha.setText("");
        txtTemCel.setText("");
        txtTemFa.setText("");
    }

    private void clic_btnSalir(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Salir");
        builder.setMessage("¿Estás seguro de que deseas salir?");

        builder.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // Nada
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

}