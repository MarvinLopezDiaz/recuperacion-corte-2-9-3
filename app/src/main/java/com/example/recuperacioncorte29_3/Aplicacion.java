package com.example.recuperacioncorte29_3;

import android.app.Application;
import Modelo.TemperaturaDB;
import java.util.ArrayList;

public class Aplicacion extends Application{
    public static ArrayList<Temperatura> listaTemp;
    private static TemperaturaDB db;
    private static Adapter adaptador;

    @Override
    public void onCreate() {
        super.onCreate();

        this.db = new TemperaturaDB(this);

        this.listaTemp = new ArrayList<>();
        this.listaTemp = db.allTemperatura();

        this.adaptador = new Adapter(this.listaTemp, this);
    }

    public static Adapter getAdaptador() {
        return adaptador;
    }

    public static void agregarTemperatura(Temperatura temp)
    {
        if(temp == null)
            return;
        if (temp.getId() == 0)
            return;

        db.insertTemperatura(temp);

        temp = db.ultimoTemperatura();
        listaTemp.add(temp);
    }

    public static void eliminarTodo()
    {
        db.deleteAllTemperatura();
    }

    public static float promedioTemp()
    {
        float promedio = 0.0f;
        int tamañoLista = listaTemp.size();

        for(int i=0; i<tamañoLista; promedio+=listaTemp.get(i).getFahrenheit(), i++);

        return promedio/tamañoLista;
    }

}
