package Modelo;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class TemperaturaDBHelper extends SQLiteOpenHelper
{
    private static final String TEXT_TYPE = " TEXT";
    private static final String CADENA_INT = " INTEGER";
    private static final String CADENA_FLOAT = " REAL";
    private static final String COMMA_SEP = ", ";

    private static final String SQL_CREATE_IMC = "CREATE TABLE " +
            DefineTabla.Temperatura.TABLE_NAME + "( " +
            DefineTabla.Temperatura.COLUMN_NAME_ID + CADENA_INT + " PRIMARY KEY" + COMMA_SEP +
            DefineTabla.Temperatura.COLUMN_NAME_FECHA + CADENA_FLOAT + COMMA_SEP +
            DefineTabla.Temperatura.COLUMN_NAME_CELSIUS + CADENA_FLOAT + COMMA_SEP +
            DefineTabla.Temperatura.COLUMN_NAME_FAHRENHEIT + CADENA_FLOAT + " );";

    private static final String SQL_DELETE_IMC = "DROP TABLE IF EXISTS " + DefineTabla.Temperatura.TABLE_NAME;

    private static final String DATABASE_NAME = "Temperatura.db";
    private static final int DATABASE_VERSION = 1;

    public TemperaturaDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_IMC);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(SQL_DELETE_IMC);
        onCreate(sqLiteDatabase);
    }

}
