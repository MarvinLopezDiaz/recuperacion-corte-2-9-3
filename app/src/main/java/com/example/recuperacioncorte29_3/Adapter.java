package com.example.recuperacioncorte29_3;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Application;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;


public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {
    private ArrayList<Temperatura> infTemp;
    private Context context;
    private View.OnClickListener listener;

    public Adapter(ArrayList<Temperatura> infTemp, Application contexto) {
        this.infTemp = infTemp;
        this.context = contexto;
        this.listener = null;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            this.textView = itemView.findViewById(android.R.id.text1);
            this.textView.setTextColor(Color.BLACK);
        }
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Temperatura temp = this.infTemp.get(position);

        String strTemp = "ID: " + temp.getId()
                + ", Fecha: " + temp.getFecha()
                + " Celsius: " + temp.getCelsius()
                + " Fahrenheit: " + temp.getFahrenheit();

        holder.textView.setText(strTemp);
        if (listener != null)
            holder.textView.setOnClickListener(this.listener);
    }

    @Override
    public int getItemCount() {
        return this.infTemp.size();
    }
}