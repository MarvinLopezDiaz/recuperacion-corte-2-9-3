package Modelo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

//import com.example.recuperacioncorte29_3.Adapter;
import com.example.recuperacioncorte29_3.Temperatura;

import java.util.ArrayList;


public class TemperaturaDB implements Persistencia, Proyeccion {
    private Context context;
    private TemperaturaDBHelper helper;
    private SQLiteDatabase db;

    public TemperaturaDB(Context context, TemperaturaDBHelper helper) {
        this.context = context;
        this.helper = helper;
    }

    public TemperaturaDB(Context context) {
        this.context = context;
        this.helper = new TemperaturaDBHelper(this.context);
        this.openDataBase();
    }

    @Override
    public void openDataBase() {
        db = helper.getWritableDatabase();
    }

    @Override
    public void closeDataBase() {
        helper.close();
    }

    @Override
    public long insertTemperatura(Temperatura temp) {
        ContentValues values = new ContentValues();
        values.put(DefineTabla.Temperatura.COLUMN_NAME_FECHA, temp.getFecha());
        values.put(DefineTabla.Temperatura.COLUMN_NAME_CELSIUS, temp.getCelsius());
        values.put(DefineTabla.Temperatura.COLUMN_NAME_FAHRENHEIT, temp.getFahrenheit());

        this.openDataBase();
        long num = db.insert(DefineTabla.Temperatura.TABLE_NAME, null, values);
        return num;
    }

    @Override
    public long updateTemperatura(Temperatura temp) {
        ContentValues values = new ContentValues();
        values.put(DefineTabla.Temperatura.COLUMN_NAME_FECHA, temp.getFecha());
        values.put(DefineTabla.Temperatura.COLUMN_NAME_CELSIUS, temp.getCelsius());
        values.put(DefineTabla.Temperatura.COLUMN_NAME_FAHRENHEIT, temp.getFahrenheit());

        this.openDataBase();
        long num = db.update(
                DefineTabla.Temperatura.TABLE_NAME,
                values,
                DefineTabla.Temperatura.COLUMN_NAME_ID + " = " + temp.getId(),
                null
        );
        return num;
    }

    @Override
    public void deleteTemperatura(int id) {
        this.openDataBase();
        db.delete(DefineTabla.Temperatura.TABLE_NAME,
                DefineTabla.Temperatura.COLUMN_NAME_ID + "=?",
                new String[]{String.valueOf(id)});
    }

    public void deleteAllTemperatura(){
        this.openDataBase();
        db.delete(DefineTabla.Temperatura.TABLE_NAME, null, null);
    }

    @Override
    public Temperatura getTemperatura(int id) {
        db = helper.getWritableDatabase();
        Cursor cursor = db.query(
                DefineTabla.Temperatura.TABLE_NAME,
                DefineTabla.Temperatura.REGISTRO,
                DefineTabla.Temperatura.COLUMN_NAME_ID + "=?",
                new String[]{String.valueOf(id)},
                null, null, null);

        cursor.moveToFirst();
        Temperatura tempe = new Temperatura();
        if (cursor.getCount() > 0)
            tempe = readTemperatura(cursor);
        cursor.close();
        return tempe;
    }

    @Override
    public ArrayList<Temperatura> allTemperatura() {
        this.openDataBase();
        Cursor cursor = db.query(
                DefineTabla.Temperatura.TABLE_NAME,
                DefineTabla.Temperatura.REGISTRO,
                null, null, null, null, null);
        ArrayList<Temperatura> TempsList = new ArrayList<>();
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Temperatura temp = readTemperatura(cursor);
            TempsList.add(temp);
            cursor.moveToNext();
        }
        cursor.close();
        return TempsList;
    }

    @Override
    public Temperatura readTemperatura(Cursor cursor)
    {
        int id = cursor.getInt(0);
        String fecha = cursor.getString(1);
        float celsius = cursor.getFloat(2);

        Temperatura temp = new Temperatura(id, celsius, fecha);

        return temp;
    }

    @Override
    public Temperatura ultimoTemperatura() {
        db = helper.getWritableDatabase();
        String query = "SELECT * FROM " + DefineTabla.Temperatura.TABLE_NAME +
                " ORDER BY " + DefineTabla.Temperatura.COLUMN_NAME_ID +
                " DESC LIMIT 1";
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        Temperatura temp = readTemperatura(cursor);
        cursor.close();
        return temp;
    }
}
