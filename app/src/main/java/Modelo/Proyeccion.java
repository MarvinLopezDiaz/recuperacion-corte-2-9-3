package Modelo;
import android.database.Cursor;

import com.example.recuperacioncorte29_3.Temperatura;

import java.util.ArrayList;

public interface Proyeccion {
    Temperatura getTemperatura(int id);
    ArrayList<Temperatura> allTemperatura();
    Temperatura readTemperatura(Cursor cursor);
    Temperatura ultimoTemperatura();

}
