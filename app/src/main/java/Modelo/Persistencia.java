package Modelo;

import com.example.recuperacioncorte29_3.Temperatura;

public interface Persistencia {
    void openDataBase();
    void closeDataBase();
    long insertTemperatura(Temperatura temp);
    long updateTemperatura(Temperatura temp);
    void deleteTemperatura(int id);

}
