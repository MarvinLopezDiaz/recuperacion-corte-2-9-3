package com.example.recuperacioncorte29_3;

public class Temperatura {

    private int id;
    private float celsius;
    private float fahrenheit;
    private String fecha;

    public Temperatura() {
        this.id = 0;
        this.celsius = 0.0f;
        this.fahrenheit = 0.0f;
        this.fecha = "";
    }
    public Temperatura(int id, float celsius, String fecha) {
        this.id = id;
        this.celsius = celsius;
        this.fecha = fecha;
        this.fahrenheit = calcularTem();
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public float getCelsius() { return celsius; }
    public void setCelsius(float celsius) { this.celsius = celsius; }

    public String getFecha() { return fecha; }
    public void setFecha(String fecha) { this.fecha = fecha; }

    public float getFahrenheit() {
        return fahrenheit;
    }

    private float calcularTem() {
        if (celsius <= 0) {
            return 0;
        }
        return (celsius * 9/5) + 32;
    }

}
