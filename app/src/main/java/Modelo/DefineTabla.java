package Modelo;

public class DefineTabla {
    public DefineTabla() {
    }

    public static abstract class Temperatura {
        public static final String TABLE_NAME = "temperatura";
        public static final String COLUMN_NAME_ID = "id";

        public static final String COLUMN_NAME_FECHA = "fecha";
        public static final String COLUMN_NAME_CELSIUS = "celsius";
        public static final String COLUMN_NAME_FAHRENHEIT = "fahrenheit";

        public static String[] REGISTRO = new String[]{
                Temperatura.COLUMN_NAME_ID,
                Temperatura.COLUMN_NAME_FECHA,
                Temperatura.COLUMN_NAME_CELSIUS,
                Temperatura.COLUMN_NAME_FAHRENHEIT
        };
    }

}
